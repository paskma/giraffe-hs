module DocumentTest(allTests) where

import Test.HUnit

import Document (getTerms)


simpleSplit = TestCase $
  assertEqual "simplest case" ["a", "b"] (getTerms "a b")

multipleSpacesSplit = TestCase $
  assertEqual "multiple spaces not yelds words" ["a", "b"] (getTerms "a  b")

dotOrSlashSplit = TestCase $ do
  assertEqual "dot" ["a", "b"] $ getTerms "a.b"
  assertEqual "slash" ["a", "b"] $ getTerms "a/b"
  --TODO:
  --assertEqual "start[" ["a", "b"] $ getWords "a[b"
  --assertEqual "end]" ["a", "b"] $ getWords "a]b"


allTests = TestLabel "Document" $ TestList [simpleSplit, multipleSpacesSplit, dotOrSlashSplit]
