module DirectoryListingTest (allTests) where

import Data.List (isSuffixOf, sort)
import Test.HUnit

import DirectoryListing (getDirectoryListing)

expectedListingSuffixes = ["a", "b", "c"]

compareListingSuffixes :: [FilePath] -> [String] -> Bool
compareListingSuffixes actualListing expectedListingSuffixes = 
  all compareSuffix (zip (sort actualListing) expectedListingSuffixes)
  where compareSuffix tuple = (snd tuple) `isSuffixOf` (fst tuple)

testListing = TestLabel "prepared listing" $ TestCase $ do 
  listing <- getDirectoryListing "data/example_listing"
  assertBool "listing" (compareListingSuffixes listing expectedListingSuffixes)

allTests = TestLabel "DirectoryListing" $ TestList [ testListing ]
