module Main where

import Test.HUnit

import TestUtil(runVerboseTests)

import qualified DirectoryListingTest
import qualified DocumentTest
import qualified IndexTest
import qualified IndexIOTest

allTests = TestList [DocumentTest.allTests, IndexTest.allTests, 
  DirectoryListingTest.allTests, IndexIOTest.allTests]

{--
  Non verbose standard mode:
>main = runTestTT allTests
--}
main = do runVerboseTests allTests
          return ()
