module IndexTest (allTests) where

import Test.HUnit
import qualified Data.Map as Map
import qualified Data.Set as Set

import Index


testEmptyNumbering = TestLabel "emptyNumbering" $ TestCase $
  [] 
  @=? 
  numberDocuments []

testOneNumbering = TestLabel "oneNumbering" $ TestCase $
  [(1, "foo")]
  @=?
  numberDocuments ["foo"]

testAddTerm = TestLabel "addTerm" $ TestCase $
  (Map.singleton "term" (Set.singleton 1))   
  @=? 
  (addTerm "term" 1 Map.empty)

testAddTermUpdate = TestLabel "addTermUpdate" $ TestCase $
  (Map.singleton "term" $ Set.fromList [2, 1])  
  @=?
  (addTerm "term" 2 (Map.singleton "term" $ Set.singleton 1))

testAddDoc = TestLabel "addDoc" $ TestCase $ do
  (Map.singleton "a" $ Set.singleton 1) @=? (addDoc (1, "a") Map.empty)
  (Map.fromList [("a", Set.singleton 1), ("b", Set.singleton 1)]) @=? (addDoc (1, "a b") Map.empty)

testInvert = TestLabel "invert" $ TestCase $
  (Map.fromList [("a", Set.singleton 1), ("b", Set.singleton 2)])  
  @=?
  (invert [(1, "a"), (2, "b")])

testLookupSet = TestLabel "lookupSet" $ TestCase $
  ["a", "b"]
  @=?
  lookupSet (Set.fromList [1, 2]) (Map.fromList [(1, "a"), (2, "b"), (3, "c")])
  
exampleDocs = ["a b c", "b a", "a c"]

testQueryTermsEmpty = TestLabel "queryTerms empty" $ TestCase $
  []
  @=?
  queryTerms [] (invertPlain exampleDocs) (plainDocListToMap exampleDocs)

testQueryTerms = TestLabel "queryTerms" $ TestCase $
  ["a b c", "b a"]
  @=?
  queryTerms ["a", "b"] (invertPlain exampleDocs) (plainDocListToMap exampleDocs)

  

allTests = TestLabel "Index" $ TestList [ testEmptyNumbering, testOneNumbering, 
 testAddTerm, testAddTermUpdate, testAddDoc,
 testInvert, testLookupSet, testQueryTermsEmpty, testQueryTerms
 ]
