module IndexIOTest (allTests) where

import System.IO.Temp(withSystemTempDirectory)
import System.FilePath(combine)
import Test.HUnit

import Index(IndexMap, invert)
import IndexIO (writeIndex, readIndex)


exampleIndex = invert [(1, "a b"), (2, "a c")]

doWriteAndLoad :: FilePath -> IO (Either String IndexMap)
doWriteAndLoad tempName = do
  writeIndex tempName' exampleIndex
  readIndex tempName'
  where tempName' = combine tempName "file"


testWriteAndRead = TestLabel "writeAndRead" $ TestCase $ do
  loaded <- withSystemTempDirectory "index-hs.tmp" doWriteAndLoad
  case loaded of
    Left  message     -> assertFailure message
    Right loadedIndex -> exampleIndex @=? loadedIndex
  
allTests = TestLabel "IndexIO" $ TestList [ testWriteAndRead ]
