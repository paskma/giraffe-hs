module Document(Document, Term, getTerms) where

import Text.Regex
import Data.Char

type Document = String
type Term = String

getTerms :: Document -> [Term]
getTerms str = 
  filter (not . null) $ splitRegex splitter (map toLower str)

--TODO: does not work for [ and ]
splitter = mkRegex "[/ \\&\\+,\\-_\\.=\\(\\)\\\\*'\"]"

--TODO: normalize words
