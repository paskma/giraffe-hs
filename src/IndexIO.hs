module IndexIO(writeIndex, readIndex, writeDocuments, readDocuments) where

import qualified Data.ByteString as B
import System.IO(FilePath)
import Data.Serialize

import Index(IndexMap, DocumentMap)

writeIndex :: FilePath -> IndexMap -> IO ()
writeIndex path index = B.writeFile path (encode index)


readIndex :: FilePath -> IO (Either String IndexMap)
readIndex path = do
  content <- B.readFile path
  return (decode content)

writeDocuments :: FilePath -> DocumentMap -> IO ()
writeDocuments path documentMap = B.writeFile path (encode documentMap)


readDocuments :: FilePath -> IO (Either String DocumentMap)
readDocuments path = do
  content <- B.readFile path
  return (decode content)
