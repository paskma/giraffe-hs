module Main where

import System.Environment (getArgs)

import Index
import IndexIO
import DirectoryListing
import Conf

query terms = do
  putStrLn "Loading index..."
  suc <- readIndex indexFile
  case suc of
    Left message -> putStrLn ("Failed to load index: " ++ message)
    Right index -> query' terms index

query' terms index = do
  putStrLn "Loading docs..."
  suc <- readDocuments docsFile
  case suc of
    Left message -> putStrLn ("Failed to load docs: " ++ message)
    Right docs -> query'' terms index docs

query'' terms index docs = do
  putStrLn ("Found " ++ (show (length result)) ++ " documents.")
  putStrLn (show result)
    where result = queryTerms terms index docs

processArguments = do
  args <- getArgs
  case args of
    [] -> putStrLn "expecting one or more arguments, the query"
    _ -> query args

main = processArguments
