module Main where

import System.Environment (getArgs)

import Index
import IndexIO
import DirectoryListing
import Conf

buildIndex directory = do
  putStrLn ("Traversing '" ++ directory ++ "'...")
  listing <- getDirectoryListing directory
  putStrLn ((show (length listing)) ++ " objects found.")
  putStrLn "Writing docs..."
  writeDocuments docsFile (docListToMap (numberDocuments listing))
  putStrLn "Inverting..."
  index <- return (invert (numberDocuments listing))
  index' <- index `seq` return index
  putStrLn "Writing index..."
  writeIndex indexFile index'
  putStrLn "Done."


processArguments = do
  args <- getArgs
  case args of
    [directory] -> buildIndex directory
    _ -> putStrLn "expecting one argument, the directory to be indexed"

main = processArguments
