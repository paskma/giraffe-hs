module DirectoryListing (getDirectoryListing) where

import System.Directory (doesDirectoryExist, getDirectoryContents)
import System.FilePath ((</>))
import Control.Monad (forM)
import System.IO.Error (tryIOError)

getDirectoryListing :: FilePath -> IO [FilePath]
getDirectoryListing topdir = do
  names <- safe_getDirectoryContents topdir
  let properNames = filter (`notElem` [".", ".."]) names
  paths <- forM properNames $ \name -> do
    let path = topdir </> name
    isDirectory <- doesDirectoryExist path
    if isDirectory
      then getDirectoryListing path
      else return [path]
  return (concat paths)

-- Catch IO exceptions raised by insufficient permissions
safe_getDirectoryContents :: FilePath -> IO [FilePath]
safe_getDirectoryContents path = do
  listingOrError <- tryIOError (getDirectoryContents path)
  case listingOrError of
    Left e -> return []
    Right listing -> return listing

