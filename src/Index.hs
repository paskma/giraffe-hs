module Index(
    IndexMap
  , numberDocuments
  , addTerm
  , addDoc
  , invert
  , DocumentMap
  , lookupSet
  , docListToMap
  , docMapToList
  , queryTerms
  , invertPlain
  , plainDocListToMap
  ) where

import qualified Data.Map as Map
import qualified Data.Set as Set
import Data.Int

import Document(Document, Term, getTerms)

type DocId = Int64
type DocIdSet = Set.Set DocId

type IndexMap = Map.Map Term DocIdSet
type DocumentMap = Map.Map DocId Document

numberDocuments :: [Document] -> [(DocId, Document)]
numberDocuments docs = zip [1..] docs

docListToMap :: [(DocId, Document)] -> DocumentMap
docListToMap = Map.fromList

-- wrapper for docListToMap
plainDocListToMap :: [Document] -> DocumentMap
plainDocListToMap = docListToMap . numberDocuments

docMapToList :: DocumentMap -> [(DocId, Document)]
docMapToList = Map.toList

-- creates "inverted list", i.e., index for given documents and their ids
invert :: [(DocId, Document)] -> IndexMap
invert docs = foldl f Map.empty docs
  where f a_map doc = a_map `seq` addDoc doc a_map

-- wrapper for invert
invertPlain :: [Document] -> IndexMap
invertPlain docs = invert (numberDocuments docs)

-- adds one doc into index
addDoc :: (DocId, Document) -> IndexMap -> IndexMap
addDoc doc a_map = foldl f a_map (getTerms (snd doc))
  where f a_map term = addTerm term (fst doc) a_map

-- adds one term into index
addTerm :: Term -> DocId -> IndexMap -> IndexMap
addTerm term docId map = Map.alter f term map
  where f value = case value of
                    Just docIds -> Just $ Set.insert docId docIds
                    Nothing     -> Just $ Set.singleton docId

-- returns a list of documents containing all required terms
queryTerms:: [Term] -> IndexMap -> DocumentMap -> [Document]
queryTerms terms indexMap docMap = lookupSet (queryAndTerms terms indexMap) docMap

-- returns a set of doc ids that containing all required tems (uses logical and)
queryAndTerms:: [Term] -> IndexMap -> DocIdSet
queryAndTerms []       _        = Set.empty
queryAndTerms andTerms indexMap = foldr f (maybeToSet  (Map.lookup (head andTerms) indexMap)) (tail andTerms)
  where f term acc = Set.intersection acc (maybeToSet (Map.lookup term indexMap))

-- converts Maybe set to set or empty set
maybeToSet:: Maybe (Set.Set a) -> Set.Set a
maybeToSet maybe = case maybe of
  Just set -> set
  Nothing -> Set.empty

-- lookups a set of keys in map, returns list of values
lookupSet:: Ord k => Set.Set k -> Map.Map k a -> [a]
lookupSet keys a_map = Set.foldr f [] keys
  where f key acc = case Map.lookup key a_map of
                      Just mapValue -> mapValue : acc
                      Nothing       -> acc








